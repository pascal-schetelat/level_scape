from __future__ import print_function
import sys, random, os
import pymunk
import numpy as np
#from skimage.transform import swirl
import pandas as pd

#from utils import BoundingBox
DEPTH = 32
FLAGS = 0
CAMERA_SLACK = 30



from levelscape.levels.landscape import create_skyline_vertices_from_serie_triangle_strip


n = 300
alt = 100 * np.abs(np.random.normal(1, 1, size=[n]))
#distance = 1000
#screen_width = DISPLAY[0]
#screen_width = DISPLAY[0]
level_length_in_meter = 1000
level_scale = 1/160. # in meters by pixels -> here 160px by meters
# the blob is 80 pixels high, ie 50cm high
level_length = level_length_in_meter/level_scale # px




def blob_vertices(x,y,radius,n):
    stress = .15

    angles = np.linspace(0, 2 * np.pi, n+1)[:-1]
    Xout = x + radius*np.cos(angles)
    Yout = y + radius*np.sin(angles)
    ratio = 0.8
    Xin = x + ratio*radius*np.cos(angles)#+np.pi/n)
    Yin = y + ratio*radius*np.sin(angles)#+np.pi/n)

    #X = np.concatenate((Xout,Xin,[x]))
    #Y = np.concatenate((Yout,Yin,[y]))

    X = np.concatenate((Xout,[x]))
    Y = np.concatenate((Yout,[y]))

    ind = np.arange(n)
    skin_out   = zip(ind,      np.roll(ind,1) ,  stress*np.ones(n)*2*np.pi*radius/n       , 2.*np.ones(n))
    skin_in    = zip(n+ind,  n+np.roll(ind, 1) , stress*np.ones(n)*ratio*2*np.pi*radius/n , 1.5*np.ones(n))
    #radial     = zip(n+ind,    (2*n)*np.ones(n),      1.2*np.ones(n)*radius*ratio             , 0.5*np.ones(n) )
    radial = zip(ind, n * np.ones(n), np.ones(n) * radius , 0.3 * np.ones(n))

    thick  =  stress*np.sqrt(((1 - ratio) * radius)**2+(2*np.pi*radius/n)**2)

    #inter_skin_radial = zip(ind, n + ind            , np.ones(n) * (1 - ratio) * radius   , 1.5*np.ones(n))
    #inter_skin_diag   = zip(ind, n + np.roll(ind, 1), np.ones(n) * thick*1.1  , 2*np.ones(n))
    #inter_skin_diag2 = zip(ind, n + np.roll(ind, -1), np.ones(n) * thick, 1 * np.ones(n))


    return list(zip(X,Y)),list(radial)+list(skin_out) #+skin_in+inter_skin_radial+inter_skin_diag#+inter_skin_diag2


def build_player(node_number, position):
    """Add a spring network to a given space at a specific position"""
    nodes = []
    shapes = []
    springs = []
    RADIUS = 40
    x0,y0 = position
    vertices_coords, connections = blob_vertices(x=x0, y=y0, radius=RADIUS, n=node_number)


    for ii,(x,y) in enumerate(vertices_coords):

        if ii < node_number :
            radius = 5
        else :
            radius = 10

        mass = 0.5  # /node_number
        inertia = pymunk.moment_for_circle(mass, 0, radius, (0, 0))
        node = pymunk.Body(mass, pymunk.inf)  # inertia)
        node.position = x, y
        shape = pymunk.Circle(node, radius, (0, 0))
        if ii == node_number :
            shape.friction = 0.
        else :
            shape.friction = 0.1
        shapes.append(shape)


        nodes.append(node)


    for ii,(i,j,dist,stiff) in enumerate(connections):

        spring = pymunk.DampedSpring(a=nodes[int(i)],b=nodes[int(j)],
                                     anchor_a=(0.0,0),anchor_b=(0,0),
                                     rest_length=dist,stiffness=800*stiff,damping=23)


        springs.append(spring)

    #space.add(*nodes+shapes+springs)

    return nodes, shapes, springs








def convert_sky_direction_to_screen(theta,phi,screen,theta_min,theta_max,phi_min,phi_max):
    """

    Angles are in radiant

    :param theta: angle from the zenith
    :param phi:  angle from south
    :return:
    """
    vertical_field_of_view = 60*np.pi/180 # radian
    horizontal_field_of_view = 90*np.pi/180  # radian
    theta_min = 0.1
    theta_max = np.pi / 2
    phi_min = -np.pi
    phi_max = np.pi * (1 / 12. + 1.)

    DISPLAY = (screen.get_width(),screen.get_height())
    vertices_ = np.vstack(
        [(phi - phi_min) / (phi_max - phi_min) *DISPLAY[0], (theta - theta_min) / (theta_max - theta_min) * DISPLAY[1]])

    return vertices_

def add_ball(space):
    """Add a ball to the given space at a random position"""
    mass = 2
    radius = 14
    inertia = pymunk.moment_for_circle(mass, 0, radius, (0,0))
    body = pymunk.Body(mass, inertia)
    x = random.randint(120,380)
    body.position = x, 550
    shape = pymunk.Circle(body, radius, (0,0))
    space.add(body, shape)
    return shape




n = 300
alt = 100 * np.abs(np.random.normal(1, 1, size=[n]))
#distance = 1000
#screen_width = DISPLAY[0]
#screen_width = DISPLAY[0]
level_length_in_meter = 100
level_scale = 1/160. # in meters by pixels -> here 160px by meters
# the blob is 80 pixels high, ie 50cm high
level_length = level_length_in_meter/level_scale # px




def rotation_matrix(theta,x0,y0):
    """
    a c tx
    b d ty
    :param theta:
    :return:
    """
    rot = np.array([[ np.cos(theta),-np.sin(theta)],
                    [ np.sin(theta), np.cos(theta)]])

    trans = np.array([[ 1, 0, x0],
                      [ 0, 1, y0]])

    return rot,trans

def static_factory(vertices):
    static = pymunk.Body(body_type=pymunk.Body.STATIC)
    shape = pymunk.Poly(static,vertices)
    return [static, shape]


def add_slider(x0,y0,L,theta,width,mass,friction,open=1):
    """


    :param space:
    :param x0,y0:  position of the opening
    :param L: length of the slider
    :param theta: angle of the slider
    :param width:
    :param friction:
    :param open: [0;1]  the initial position of the slider
    :return:
    """

    b = pymunk.Body(mass, pymunk.inf)

    x1,y1 = x0+L*np.cos(theta), y0+L*np.sin(theta)

    #b.position = 0,0#0.5*(x0+x1),0.5*(y0+y1)

    x_lower  = (1-open)*x0 + open*x1
    #x_upper  = x_lower + (x1-x0)

    y_lower  = (1-open)*y0 + open*y1
    #y_upper  = y_lower + (y1-y0)

    vertices = np.array([(L * open, -width / 2.),
                         (L * open, width / 2.),
                         (L * (open + 1), width / 2.),
                         (L * (open + 1), -width / 2.),
                         ]).T
    # vertices = np.array([(L * open, -width / 2.),
    #                      (L * (open + 1), -width / 2.),
    #                      (L * (open + 1), width / 2.),
    #                      (L * open, width / 2.),
    #                      ]).T

    rot_mat,trans_mat = rotation_matrix(theta, x0,y0)
    rotated_vertices  = rot_mat.dot(vertices)

    translated_vertices = trans_mat.dot(np.vstack([rotated_vertices, np.ones(shape=[1, vertices.shape[1]])])).T
    sb = pymunk.Poly(b,translated_vertices)
    sb.friction = friction

    static = pymunk.Body(body_type=pymunk.Body.STATIC)
    move_joint = pymunk.GrooveJoint(static, b, (x0, y0), (x1, y1), (x_lower,y_lower))

    return b,sb, move_joint,translated_vertices

def slider_from_vertices(vertices):
    """

    :param vertices: must be a rectangle described by 4 successives vertices
    :return:
    """
    vertices = np.array(vertices)
    a = np.linalg.norm(vertices[0,:]-vertices[1,:])
    b = np.linalg.norm(vertices[1, :] - vertices[2, :])

    mass = 1

    if a > b :
        L = a
        width = b
        start = 0.5 * (vertices[1, :] + vertices[2, :])
        end   = 0.5 * (vertices[3, :] + vertices[0, :])

    else :
        L = b
        width = a
        start = 0.5 * (vertices[0, :] + vertices[1, :])
        end   = 0.5 * (vertices[2, :] + vertices[3, :])

    if start[1]>end[1]:
        start,end = end,start

    x0,y0 = start
    theta = np.arctan2(*(end-start)[::-1])


    return x0, y0, L, theta, width, mass


def build_circle(x,y,radius,n):
    angles = np.linspace(0, 2 * np.pi, n+1)[:-1]
    Xout = x + radius*np.cos(angles)
    Yout = y + radius*np.sin(angles)
    X = np.concatenate((Xout,[x]))
    Y = np.concatenate((Yout,[y]))

    return np.array(list(zip(X,Y))).ravel()

def create_obstacle(x,y,w,h,body_type="static",mass=1,texture_object=None):
    """Add a ball to the given space at a random position"""
    mass = 2
    radius = 14
    inertia = pymunk.moment_for_circle(mass, 0, radius, (0,0))
    body = pymunk.Body(mass, inertia)
    x = random.randint(120,380)
    body.position = x, 550
    shape = pymunk.Circle(body, radius, (0,0))
    return [shape]+[body]


    return bodies+shapes

def create_terrain(terrain_coords):
    """

    :param terrain_coords: a 2D array of coords in meters (length, altitude)
    :return:
    """

    n = 50
    #vertices = 100*np.abs(np.random.normal(1, 0.1, size=[n]))
    segment_radius = 50
    vertices, scale = create_skyline_vertices_from_serie_triangle_strip( 0*np.abs(np.random.normal(1, 0.05, size=[n])), 0, terrain_length/level_scale, level_scale, DISPLAY[0],
                                                                          DISPLAY[1])
    coords = np.copy(np.reshape(vertices,[-1,2])[1::2,:])
    # translating in pymunk coordinates
    # todo take into account the angle of the segment
    coords[:,1] = DISPLAY[1]-coords[:,1]-(segment_radius)
    #coords = zip(np.linspace(0,terrain_width,n),vertices[1:2:])
    segments = zip(coords[:-1],coords[1:])
    #zip(coords.tolist()[:-1],coords.tolist()[1:])
    shapes = []
    for i,seg in enumerate(segments):
        if np.random.rand()> 1: #np.randint()
            body = pymunk.Body(body_type=pymunk.Body.STATIC)
            shape = pymunk.Segment(body, seg[0], (seg[0][0],seg[0][1]+600), segment_radius)
            shape.friction = 0.8
            shapes.append(shape)
            space.add(shape)

        body = pymunk.Body(body_type=pymunk.Body.STATIC)
        shape = pymunk.Segment(body, seg[0],seg[1], segment_radius)
        shape.friction = 1.
        shapes.append(shape)
        space.add(shape)
    #ground = [(coords[0][0],-1000)]+coords+[(coords[-1][0],-1000)]

    # Ymin = -1000
    #
    # X = np.repeat(np.linspace(0,terrain_width,n),2)
    # Y = np.vstack([Ymin*np.ones(n),(vertices+50)]).T.ravel()
    # ground = np.vstack([X,Y]).T#.ravel()#zip(X,Y)

    return shapes, vertices


def build_tank(x,y):

    # frame_vertices = [(150.,150.),(250.,150.),(250.,170.),(150.,170.)]
    frame_vertices = [(-150., -20.), (250., -20.), (250., 20.), (-150., 20.)]
    frame_mass = float(500.)
    frame_moment = pymunk.moment_for_poly(mass=frame_mass, vertices=frame_vertices)
    frame_body = pymunk.Body(frame_mass, frame_moment)
    frame_body.position = (200+x, 200+y)
    # frame_body = pymunk.Body(0,0)
    frame_shape = pymunk.Poly(frame_body, frame_vertices)
    frame_shape.friction = 0.6

    wheel_mass = 200
    wheel_moment = pymunk.moment_for_circle(wheel_mass, 10, 20)

    wheel_1_body = pymunk.Body(wheel_mass, wheel_moment)
    wheel_1_body.position = (20+x, 130+y)
    wheel_1_shape = pymunk.Circle(wheel_1_body, 30)
    wheel_1_shape.friction = 0.99

    wheel_2_body = pymunk.Body(wheel_mass, wheel_moment)
    wheel_2_body.position = (480+x, 130+y)
    wheel_2_shape = pymunk.Circle(wheel_2_body, 30)
    wheel_2_shape.friction = 0.99

    wheel_3_body = pymunk.Body(wheel_mass, wheel_moment)
    wheel_3_body.position = (125+x, 100+y)
    wheel_3_shape = pymunk.Circle(wheel_3_body, 60)
    wheel_3_shape.friction = 0.99

    wheel_4_body = pymunk.Body(wheel_mass, wheel_moment)
    wheel_4_body.position = (375+x, 100+y)
    wheel_4_shape = pymunk.Circle(wheel_4_body, 60)
    wheel_4_shape.friction = 0.99

    wheel_5_body = pymunk.Body(wheel_mass, wheel_moment)
    wheel_5_body.position = (250+x, 100+y)
    wheel_5_shape = pymunk.Circle(wheel_5_body, 60)
    wheel_5_shape.friction = 0.99

    # wheel_5_body = pymunk.Body(wheel_mass,wheel_moment)
    # wheel_5_body.position = (280,120)
    # wheel_5_shape = pymunk.Circle(wheel_2_body,20)
    # wheel_5_shape.friction = 0.9

    wheel_1_rotation_center_joint = pymunk.PivotJoint(frame_body, wheel_1_body,
                                                      wheel_1_body.position)  # , (-60,-10), (0,0)) # 3
    wheel_2_rotation_center_joint = pymunk.PivotJoint(frame_body, wheel_2_body,
                                                      wheel_2_body.position)  # , (-60,-10), (0,0)) # 3

    wheel_3_rotation_center_joint = pymunk.PivotJoint(frame_body, wheel_3_body,
                                                      wheel_3_body.position)  # , (-60,-10), (0,0)) # 3

    wheel_4_rotation_center_joint = pymunk.PivotJoint(frame_body, wheel_4_body,
                                                      wheel_4_body.position)  # , (-60,-10), (0,0)) # 3

    wheel_5_rotation_center_joint = pymunk.PivotJoint(frame_body, wheel_5_body,
                                                      wheel_5_body.position)  # , (-60,-10), (0,0)) # 3

    motor1 = pymunk.SimpleMotor(frame_body, wheel_3_body, rate=0.)
    motor2 = pymunk.SimpleMotor(frame_body, wheel_4_body, rate=0.)
    motor3 = pymunk.SimpleMotor(frame_body, wheel_5_body, rate=0.)
    seg_bodies = []
    seg_shapes = []
    seg_joints = []
    thetas = np.linspace(0, 2 * np.pi, 80)[:-1]
    seg_mass = float(200.0)
    seg_moment = pymunk.moment_for_segment(seg_mass, (-5, -5), (5, 5), 2)


    for i, theta in enumerate(thetas):
        next_ = np.mod(i + 1, len(thetas))
        x0 = 250+x + 350 * np.cos(thetas[i])
        y0 = 100+y + 70 * np.sin(thetas[i])
        x1 = 250+x + 350 * np.cos(thetas[next_])
        y1 = 100+x + 70 * np.sin(thetas[next_])

        seg_body = pymunk.Body(seg_mass, seg_moment)
        seg_body.position = (x0, y0)  # (0.5*(x0+x1),0.5*(y0+y1))
        seg_shape = pymunk.Segment(seg_body, (-5 * np.sin(theta), 5 * np.cos(theta)),
                                   (5 * np.sin(theta), -5 * np.cos(theta)), 2)
        seg_shape.friction = 0.99
        seg_bodies.append(seg_body)
        seg_shapes.append(seg_shape)

    for i, seg in enumerate(seg_bodies):
        next_ = np.mod(i + 1, len(thetas))
        anchor_a = (seg_shapes[i].a[0] - 0.2 * (seg_shapes[i].b[0] - seg_shapes[i].a[0]),
                    seg_shapes[i].a[1] - 0.2 * (seg_shapes[i].b[1] - seg_shapes[i].a[1]),)

        anchor_b = (seg_shapes[next_].b[0] - 0.2 * (seg_shapes[next_].a[0] - seg_shapes[next_].b[0]),
                    seg_shapes[next_].b[1] - 0.2 * (seg_shapes[next_].a[1] - seg_shapes[next_].b[1]),)

        seg_joint = pymunk.PivotJoint(seg, seg_bodies[next_], anchor_a,
                                      anchor_b)
        # seg_joint = pymunk.PivotJoint(seg, seg_bodies[next_],(15*np.sin(thetas[next_]),-15*np.cos(thetas[next_])))
        # seg_joint = pymunk.PinJoint(seg, seg_bodies[next_], (-5*np.sin(theta),5*np.cos(theta)),
        #                            (5*np.sin(thetas[next_]),-5*np.cos(thetas[next_])))
        # (seg_bodies[i + 1].position[0]-2,seg_bodies[i+1].position[1]),
        # 3,10)
        seg_joints.append(seg_joint)

    shapes = [  frame_shape,
                wheel_1_shape,
                wheel_2_shape,
                wheel_3_shape,
                wheel_4_shape,
                wheel_5_shape,
                ]+seg_shapes
    constrains = [
                 wheel_1_rotation_center_joint,
                 wheel_2_rotation_center_joint,
                 wheel_3_rotation_center_joint,
                 wheel_4_rotation_center_joint,
                 wheel_5_rotation_center_joint,] +  seg_joints + [motor1, motor2, motor3]

    bodies = [frame_body,
              wheel_1_body,
              wheel_2_body,
              wheel_3_body,
              wheel_4_body,
              wheel_5_body,
              ] + seg_bodies



    #space.add(*bodies)
    return bodies, shapes, constrains, {'frame':[frame_shape],'wheels': [wheel_1_shape,wheel_2_shape,wheel_3_shape,wheel_4_shape,wheel_5_shape],'tracks':seg_shapes}

if __name__ == '__main__':

    sky = Sky()
