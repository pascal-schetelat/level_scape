import geopandas as gpd
import requests
import os
import json
import pandas as pd
from shapely.geometry import Polygon
import numpy as np




def get_autocomplete(text):
    r = requests.get("https://api-adresse.data.gouv.fr/search/?",params={'q':text})
    return json.loads(r)


class Adresse(dict):

    def __init__(self):

        # self.ip.Completer.debug =False
        # python.set_hook('complete_command', adress_completer, re_key='.*ad')
        pass

    def get_autocomplete(self, text):
        r = requests.get("https://api-adresse.data.gouv.fr/search/?", params={'q': text})
        self.proposals = json.loads(r.content)
        if 'features' not in self.proposals:
            return []
        return [feature['properties']['label'] for feature in self.proposals['features']]

    def get_instance_name(self, text):
        return [k for k, v in globals().items() if v is self]

    def _ipython_key_completions_(self):

        text = get_ipython().Completer.line_buffer # "adrs['amiens"#
        instance_names = self.get_instance_name(text)[0]
        key = text.replace('"', "'").split(instance_names + "['")[-1]
        # raise(Exception((text,key)))

        # return
        # print(self.ip.Completer.line_buffer)
        out = self.get_autocomplete(key)

        return out + list(self.keys())


    def __getitem__(self, key):
        key = self.get_autocomplete(key)[0]
        feature = self.proposals['features'][0]
        self.__setitem__(key, feature)
        return feature

from pkg_resources import resource_filename

def load_grenoble():
    path = resource_filename('levelscape','/levels/elevations_hd.hdf')
    return pd.read_hdf(path, key='all').query('z>0')

#
# def define_cicles(center, radius):
#     n = 100
#
#     r = np.logspace(np.log10(1000), np.log10(50e3), num=n)
#     theta = np.linspace(0, 2 * np.pi, num=20, endpoint=False)
#     r = np.reshape(r, [1, len(r)])
#     theta = np.reshape(theta, [len(theta), 1])
#     x = r.T.dot(np.cos(theta.T))
#     y = r.T.dot(np.sin(theta.T))
#
#     crs = {'init': 'epsg:2154'}
#     circles = gpd.GeoDataFrame(index=r.ravel(),
#                                crs=crs,
#                                geometry=[Polygon(zip(shp.coords[0][0] + x[i, :], shp.coords[0][1] + y[i, :])) for i in
#                                          range(n)]).to_crs(epsg='4326')
#
#
# def get_elevation_line(line):