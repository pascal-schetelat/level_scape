# coding: utf-8
import numpy as np
import pandas as pd
import pygame
#import scipy
#from scipy.spatial import Delaunay


def create_skyline_vertices_from_serie(altitudes,distance,level_length,level_scale,screen_width,screen_height):
    """
        Create a vertex list of a skyline



    :param serie: list of point to draw
    :param altitude: relative to level corner (left,top). in meters
    :param distance: in meters
    :param shift:
    :param level_size: in pixel
    :param level_scale: in meters by pixel
    :param screen width : in pixels
    :return:
    """
    vertical_field_of_view = 60*np.pi/180 # radian
    horizontal_field_of_view = 90*np.pi/180  # radian

    # plane where skyline_scale is 1
    neutral_distance = level_scale * screen_width / (2**np.tan(horizontal_field_of_view/2))

    distance += neutral_distance

    skyline_total_length = 2*distance*np.tan(horizontal_field_of_view/2)+level_length*level_scale
    #(skyline_total_length)
    skyline_length_in_FOV = 2*distance*np.tan(horizontal_field_of_view/2)
    #print(skyline_length_in_FOV)
    skyline_pixels_by_meter = screen_width/skyline_length_in_FOV  # in pixel by meters
    skyline_pixel_lengths = skyline_total_length * skyline_pixels_by_meter

    # how many pixel the background move when the foreground move by 1 pixel
    skyline_scale = level_scale * skyline_pixels_by_meter

    # how many pixel does the background move when the camera move by one pixel
    skyline_perspective_scale =  2*distance*np.tan(horizontal_field_of_view/2)

    vertices = list(zip(np.linspace(start=0,stop=skyline_pixel_lengths,num=len(altitudes)).tolist(),(screen_height-altitudes*skyline_pixels_by_meter).ravel().tolist()))
    vertices = [(0,screen_height+1000)] + vertices + [(vertices[-1][0],screen_height+1000)]

    return np.array(vertices), skyline_scale

def create_circular_skyline(altitudes):
    """
    Generate vertices along a triangle strip used to draw the altitude as a polygon with a flat base at altitude 0
        a line that follow the elevation line given in altitudes.
        x is in radian, y is in m


    :param altitudes: altitudes is assumed to describe elevation (in m) along a a full circle that start in the south and
        rotate clockwise

    :return: a list of vertices

    """


    X = np.repeat(np.linspace(start=-np.pi, stop=np.pi, num=len(altitudes)), 2)
    Y = np.vstack([np.zeros(len(altitudes)),
                   (altitudes)]).T.ravel()
    vertices = np.vstack([X, Y]).T.ravel()  # zip(X,Y)

    return vertices



def create_skyline_vertices_from_serie_triangle_strip(altitudes, distance, level_length, level_scale, screen_width, screen_height):
    """
        Create a vertex list of a skyline

    return a list of vertices in screen coordinates

    :param serie: list of point to draw
    :param altitude: relative to level corner (left,top). in meters
    :param distance: in meters
    :param shift:
    :param level_size: in pixel
    :param level_scale: in meters by pixel
    :param screen width : in pixels

    :return: vertices in screen coordinates, skyline scale for shift
    """
    vertical_field_of_view = 60 * np.pi / 180  # radian
    horizontal_field_of_view = 90 * np.pi / 180  # radian

    neutral_distance = level_scale * screen_width / (2**np.tan(horizontal_field_of_view/2))

    distance += neutral_distance

    skyline_total_length = 2 * distance * np.tan(horizontal_field_of_view / 2) + level_length * level_scale
    # (skyline_total_length)
    skyline_length_in_FOV = 2 * distance * np.tan(horizontal_field_of_view / 2)
    # print(skyline_length_in_FOV)
    skyline_pixels_by_meter = screen_width / skyline_length_in_FOV  # in pixel by meters

    # how many pixel the background move when the foreground move by 1 pixel
    if distance == 0:
        skyline_pixels_by_meter = 1
        skyline_pixel_lengths = skyline_total_length
        skyline_pixels_by_meter = 1./level_scale

    else :
        skyline_pixel_lengths = skyline_total_length * skyline_pixels_by_meter

    skyline_scale = level_scale * skyline_pixels_by_meter

    # how many pixel does the background move when the camera move by one pixel
    skyline_perspective_scale = 2 * distance * np.tan(horizontal_field_of_view / 2)
    Ymin = -10000
    X = np.repeat(np.linspace(start=0, stop=skyline_pixel_lengths, num=len(altitudes)),2)
    Y = np.vstack([screen_height-Ymin*np.ones(len(altitudes)),(screen_height - altitudes * skyline_pixels_by_meter)]).T.ravel()
    vertices = np.vstack([X,Y]).T.ravel()#zip(X,Y)



    return vertices, skyline_scale

    # pygame.draw.polygon()


if __name__ == '__main__':
    n = 100
    alt = 100*np.abs(np.random.normal(1,1,size=[n]))
    distance = 1000
    screen_width = 1600
    level_length = 20000 # px
    level_scale = 1000./level_length #
    vertices,scale = create_skyline_vertices_from_serie(alt,distance,level_length,level_scale,screen_width)
    print(vertices)
    print(scale)