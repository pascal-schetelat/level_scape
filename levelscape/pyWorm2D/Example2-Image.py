from worm2d import Engine, Viewport
import pygame
import numpy as np


def make_vertices(x,y,radius=10,n=5):

    angles = np.linspace(0, 2 * np.pi, n+1)[:-1]
    Xout = [x] + (x + radius*np.cos(angles)).tolist()
    Yout = [y] + (y + radius*np.sin(angles)).tolist()
    Xout.append(Xout[1])
    Yout.append(Yout[1])
    return np.vstack([Xout, Yout]).T.ravel()


def main():
    
    engine = Engine(1920,1080, 32, False, "Worm2D")
    view = Viewport(0,0,1,1, 100.0, 75.0)    


    #loads an image
    img_ship = engine.LoadImage("example_data/ship.png")

    #loads anouther image.
    img_bg = engine.LoadImage("example_data/background.png")
    tree  = engine.LoadImage("tree-silhouette-2-2.png")

    r = 0.0
    s = 1.0
    phi = 0
    phi_0 = 0

    ticks = engine.GetTicks()
    clock = pygame.time.Clock()
    poly = make_vertices(0,0,radius=10,n=5)
    poly_v_gl = engine.LoadPolygon(poly)
    
    while engine.IsRunning():
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    return




        #get the time between frames for framerate independence
        delta = engine.GetTicks() - ticks
        ticks = engine.GetTicks()

        #increment rotation and scale
        r += 0.5*delta
        if r > 360:
            r = 0

        s += 0.01*delta
        if s > 20:
            s = 0
        phi_0 += delta
        phi = np.mod(phi_0/50.,2*np.pi)

        #start rendering
        engine.PreRender(0, 0, 0)
        engine.SetView(view, 0.0,0.0,1.0)

        #draw the non-power-of-two background
        n = 400
        for i in range(n):
            theta = i*1.0/n *2*np.pi
            #engine.DrawImage(img_ship, 20*np.cos(theta+phi),
            #                           20*np.sin(theta+phi), 10, 10, 1.0, (theta+phi)*180./np.pi)
            #engine.DrawQuad(20*np.cos(theta+phi),20*np.sin(theta+phi),8,1,0.5,0,78,200,(theta+phi)*180./np.pi, z = 0)
            engine.DrawPoly(poly_v_gl, 20*np.cos(theta+phi/100),20*np.sin(theta+phi/100),1,1,0.5,0,
                            0,78,200,
                             z = 0,mode='GL_TRIANGLE_FAN')
            #engine.DrawImage(img_bg, 20*np.cos(+phi),0, 100,100,1,0)
        #
        # #draws the image:   image  x y   w  h    a  rotation
        # engine.DrawImage(img_ship, 0,0, 10, 10, 1.0, 0)
        #
        # draw first image again, rotated by r
        #engine.DrawImage(tree,   -20,0, s*5, s*5, 1.0, 0)
        #
        # #draw first image again, scaled by s
        # engine.DrawImage(img_ship, 20,0, s, s, 1.0, s)
        clock.tick(100)
        
        engine.PostRender()
        pygame.display.set_caption("fps: " + str(clock.get_fps()))




main()
