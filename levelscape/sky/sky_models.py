# coding: utf-8
import numpy as np
from colorpy import colormodels,  ciexyz #, illuminants,



MY = np.array([[0.1787, -1.4630],
                [-0.3554, 0.4275],
                [-0.0227, 5.3251],
                [0.1206, -2.5771],
                [-0.0670, 0.3703]])

Mx = np.array([[-0.0193, -0.2592],
                [-0.0665, 0.0008],
                [-0.0004, 0.2125],
                [-0.0641, -0.8989],
                [-0.0033, 0.0452]])

My = np.array([[-0.0167, -0.2608],
                [-0.0950, 0.0092],
                [-0.0079, 0.2102],
                [-0.0441, -1.6537],
                [-0.0109, 0.0529]])


Mxz = np.array([
[0.00166,-0.00375, 0.00209,   0.],
[-0.02903, 0.06377, -0.03202, 0.00394],
[0.11693, -0.21196, 0.06052, 0.25886],
])

Myz = np.array([
[0.00275, -0.00610, 0.00317, 0,],
[-0.04214, 0.08970, -0.04153, 0.00516],
[0.15346, -0.26756, 0.06670, 0.26688],
])

def sky_color(theta, gamma, theta_sun,turbidity):
    """
    The two input variables are θ, which is the angle between the zenith and the viewing direction,
    and γ, the angle between the sun and the viewing direction.

    Turbidity defines the haziness of a fluid, see Figure 2.3. In the case of our atmosphere, it
    describes the amount of haze aerosols, which consist of particles that are relatively large. Due to
    their size, these haze particles make the air less clear, which is why we call atmosphere without
    any haze particles pure air. The turbidity is given by the following formula.
    T =
    tm + th
    tm
    The turbidity T depends on the ratio of the optical thickness of the haze atmosphere –
    consisting of both molecules (tm) and haze (th) – to that of the atmosphere with molecules only
    (tm). Note that turbidity varies with wavelength, and its value at λ = 550nm is often used for
    applications concerning visible light.

    :param theta: np.array [n,1]
    :param gamma: np.array [n,1]
    :param turbidity:  belongs to  [1 -> 1000[, 1 being clear sky, 1000 looking into a swamp
    :return:

    """
    Yrel_x, Yrel_y, Yrel_Y = Preetham_Perez_coeff(turbidity)
    theta_sun2 = theta_sun**2
    theta_sun3 = theta_sun**3
    theta_sun_ = np.vstack([theta_sun3,theta_sun2,theta_sun,1])

    T = turbidity
    T2 = T**2
    T_ = np.array([[T2,T,1]])
    Yz = (4.0453 * T - 4.9710) * np.tan((4 / 9. - T / 120.) * (np.pi - 2 * theta_sun)) - 0.2155 * T + 2.4192
    xz = T_.dot(Mxz.dot(theta_sun_))
    yz = T_.dot(Myz.dot(theta_sun_))

    return np.vstack([Yrel_x(theta,gamma,theta_sun)*xz,
                      Yrel_y(theta,gamma,theta_sun)*yz,
                      Yrel_Y(theta,gamma,theta_sun)*Yz
                    ])

def Preetham_Perez_coeff(turbidity):
    """

    :param turbidity:
    :return:
    """
    a = np.array([[turbidity],[1]])
    Ax, Bx, Cx, Dx, Ex = Mx.dot(a)
    Ay, By, Cy, Dy, Ey = My.dot(a)
    AY, BY, CY, DY, EY = MY.dot(a)

    Yrel_x = Yrel_factory(Ax, Bx, Cx, Dx, Ex)
    Yrel_y = Yrel_factory(Ay, By, Cy, Dy, Ey)
    Yrel_Y = Yrel_factory(AY, BY, CY, DY, EY)


    return Yrel_x, Yrel_y, Yrel_Y



def Yrel_factory(A,B,C,D,E):
    """
    Since the Perez model [PSM93] is the basis for both methods, properly understanding it is key in
    comprehending the models that were built on it. Originally devised in 1993 as a generalization
    of the CIE standard sky formula [CIE94], the Perez model presents a framework for describing
    the luminance of a certain sky element.
    First, the relative luminance Yrel of a sky element is determined based on five coefficients,
    which can be altered to match the current luminance distribution. The formula for this takes
    two variables as input, and is defined as follows.
        The aforementioned five coefficients are given by A through E. A darkening or brightening
    of the horizon is described by the first coefficient – A. A negative value signifies a brighter area
    around the horizon relative to the zenith luminance. Positive values correspond to a darker
    region near the horizon. Its absolute value defines how much the luminance varies between the
    zenith and horizon.
    Coefficient B on the other hand, describes the smoothness of the gradient that is caused by
    this darkening or brightening at the horizon. The absolute value of B is proportional to the
    breadth of this gradient. Higher absolute values correspond to a more gradual transition, while
    low absolute values result in a narrow band.
    The relative intensity of the area near the sun is described by coefficient C. Higher values
    correspond to this circumsolar region being perceived brighter, and a higher relative luminance
    is measured.
    The width of the circumsolar region is modulated by coefficient D. Quite similar to B,
    it determines how gradually the luminance varies between the region around the sun and the
    rest of the sky. Lower absolute values result in a smaller region, with a more sudden peak of
    intensity.
    11
    Finally, coefficient E corresponds to the relative intensity of backscattered light, i.e. the
    amount of light that scatters back from the earth’s surface. Naturally, higher values result in
    higher levels of relative luminance.
    :param turbidity:
    :return:
    """




    def Yrel(theta,gamma,theta_sun):
        """
        The two input variables are θ, which is the angle between the zenith and the viewing direction,
    and γ, the angle between the sun and the viewing direction.
    The denominator denotes the relative zenith luminance, and because of this, the angle with
the zenith θ is 0, while the angle with the sun γ equals the angle between the zenith and the
sun, here defined as θs. T
        Perez equation
        :param theta: np.array [1,n]
        :param gamma: np.array [1,n]
        :return:


        """
        Yrel_0 = (1+A*np.exp(B/np.cos(0    )))*(1+C*np.exp(D*theta_sun)+E*(np.cos(theta_sun))**2)
        #Yrel_0 = 1
        #Yrel   = (1+A*np.exp(B/np.cos(theta)))*(1+C*np.exp(D*gamma    )+E*(np.cos(gamma    ))**2)
        Yrel = (1 + A * np.exp(B / np.maximum(np.cos(theta),1e-2))) * (1 + C * np.exp(D * gamma) + E * (np.cos(gamma)) ** 2)

        return Yrel/Yrel_0

    return Yrel




def sph2cart(az, el, r):
    rcos_theta = r * np.cos(el)
    x = rcos_theta * np.cos(az)
    y = rcos_theta * np.sin(az)
    z = r * np.sin(el)
    return x, y, z

def optical_depth(theta_sun):
    theta_sun_ = theta_sun * 180 / np.pi
    # Relative optical mass
    m = 1 / (np.cos(theta_sun) + 0.18 * (93.885 - theta_sun) ** (-1.253))
    return  m

def atmospheric_transmittance(optical_depth_,turbidity):
    """

    :param optical_depth
    :return:
    """
    beta = 0.04608*turbidity-0.04586
    # Relative optical mass
    alpha = 1.3
    l = 0.35 # cm
    w = 2    # cm

    # transmittance
    def tau_r(LAMBDA):
        """
        raysleigh scaterring transmittance
        :param LAMBDA: wave length in µm
        :return:
        """
        return np.exp(-0.008735*LAMBDA**(-4.08*optical_depth_))


    def tau_alpha(LAMBDA):
        """
        Angstrom’s turbidity formula for aerosol
        :param LAMBDA: wave length in µm
        :return:
        """
        return np.exp(-beta * LAMBDA ** (-alpha * optical_depth_))

    return tau_r, tau_alpha
   # def tau_o(LAMBDA):
    #     """
    #     transmittance due to ozone absorption
    #     :param LAMBDA: wave length in µm
    #     :return:
    #     """
    #     return np.exp(-beta * LAMBDA ** (-alpha * optical_depth))
    #
    # def tau_g(LAMBDA):
    #     """
    #      transmittance due to mixed gases absorption
    #     :param LAMBDA: wave length in µm
    #     :return:
    #     """
    #     return np.exp(-beta * LAMBDA ** (-alpha * optical_depth))
    #
    #
    # def tau_w(LAMBDA):
    #     """
    #      transmittance due to water vapor absorption
    #     :param LAMBDA: wave length in µm
    #     :return:
    #     """
    #     return np.exp(-beta * LAMBDA ** (-alpha * optical_depth))

def sun_color(theta_sun,turbidity):
    """
    Issue in Python 3 in the illuminant module. Need to be patched
    :param theta_sun:
    :param turbidity:
    :return:
    """
    tau_r,tau_alpha = atmospheric_transmittance(optical_depth(theta_sun), turbidity)
    D65 = illuminants.get_illuminant_D65()

    LAMBDA = D65[:,0]/1000.
    attenuated_spectra = D65[:,1]*tau_alpha(LAMBDA)*tau_r(LAMBDA)
    attenuated_spectra = np.vstack([D65[:,0],attenuated_spectra]).T
    xyz = ciexyz.xyz_from_spectrum(attenuated_spectra)
    return np.clip(np.array([colormodels.rgb_from_xyz(xyz)]),0,1),np.sum(xyz)



def color_regularisaton(colors):
    tau = 0.4
    norm = 1/(1-np.exp(-1/tau))
    return colors/(1-np.exp(-colors/tau))/norm


def haversine_np_grid(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the unit sphere  (specified in radian)

    All args must be of equal length.

    """
    lat1 = np.pi/2-lat1
    lat2 = np.pi/2-lat2
    #lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = np.sin(dlat/2.0)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2.0)**2

    c = 2 * np.arcsin(np.sqrt(a))

    return c

def simulate_color(theta,phi,sun_theta_phi,turbidity):

    if sun_theta_phi[0] > np.pi/2 :
        colors_xyz = np.zeros(shape=(theta.shape[0],3))
        x = np.abs(sun_theta_phi[0]-np.pi/2)
        sigmoid = 300*x/(1+300*x)
        colors_xyz[:,2] = 0.8*np.sin(theta)*np.cos(x)*sigmoid*np.cos(x*2)
        colors = colormodels.rgb_from_xyz(colors_xyz.T)
        lum = 1
        return colors, colors_xyz, lum

    gamma = haversine_np_grid(phi,theta,sun_theta_phi[1],sun_theta_phi[0])

    colors_xyY  = sky_color(theta.ravel(), gamma.ravel(), sun_theta_phi[0], turbidity=turbidity)
    #sun_color_rgb,Y_sun  = sun_color(sun_theta_phi[0], turbidity)
    #print(np.mean(colors_xyY[2]))
    mean_lum =np.mean(colors_xyY[2])
    #print(mean_lum)
    #lum = mean_lum *( 0.8+40/(1+np.exp((mean_lum-6)/1.)))
    #print(mean_lum)
    lum = np.max([0.7,mean_lum])*1.3
    #print(lum)
    #lum = np.mean(colors_xyY[2])
    colors_xyz  = colormodels.xyz_color_from_xyY(colors_xyY[0], colors_xyY[1], colors_xyY[2]/lum)
    #colors_xyz = colormodels.xyz_normalize_Y1(colors_xyz)
    colors = colormodels.rgb_from_xyz(colors_xyz)  # /255.
    #colors = color_regularisaton(colors)

    colors = np.clip(colors, 0, 1)

    return colors, colors_xyz.T, lum


import itertools


def haversine_np(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the unit sphere  (specified in radian)

    All args must be of equal length.

    """
    lat1 = lat1
    lat2 = lat2
    # lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = np.sin(dlat / 2.0) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2.0) ** 2

    c = 2 * np.arcsin(np.sqrt(a))

    return c


def gnomonic_projection(theta, phi, theta_0, phi_0):
    """
    from http://mathworld.wolfram.com/GnomonicProjection.html
    :param theta:
    :param phi: lon
    :param theta_0:
    :param phi_0:
    :return:
    """
    # c = haversine_np(phi, theta, phi_0, theta_0)
    # cos_c = np.cos(c)

    cos_theta = np.cos(theta)
    sin_theta = np.sin(theta)

    sin_phi = np.sin(phi - phi_0)
    cos_phi = np.cos(phi - phi_0)

    cos_c = np.sin(theta_0) * sin_theta + np.cos(theta_0) * cos_theta * cos_phi

    x = cos_theta * sin_phi / cos_c
    y = (np.cos(theta_0) * sin_theta - np.sin(theta_0) * cos_theta * cos_phi) / cos_c

    return x, y


def inv_gnomonic_projection(x, y, theta_0, phi_0):
    rho = np.sqrt(x ** 2 + y ** 2)
    c = np.arctan(rho)
    sin_c = np.sin(c)
    cos_c = np.cos(c)

    theta = np.arcsin(cos_c * np.sin(theta_0) + y * sin_c * np.cos(theta_0) / rho)
    phi = phi_0 + np.arctan2(x * sin_c, (rho * np.cos(theta_0) * cos_c - y * np.sin(theta_0) * sin_c))
    # theta = np.sign(theta)*(np.pi/2 -np.abs(theta-np.sign(theta)*np.pi/2))
    return theta, phi

def screen_projection(height,width,screen,theta, phi,theta_min,theta_max,phi_min,phi_max, theta_0, phi_0) :
    # height = screen.get_height()
    # width = screen.get_width()

    vertical_fov = np.array([theta_min, theta_max])
    horizontal_fov = np.array([phi_min, phi_max])

    y_lim = np.tan(vertical_fov)
    x_lim = np.tan(horizontal_fov)

    x, y = gnomonic_projection(theta=np.array(theta), phi=np.array(phi), theta_0=0, phi_0=np.pi)

    X = (x - x_lim[0]) / (x_lim[1] - x_lim[0]) * width
    Y = (y - y_lim[0]) / (y_lim[1] - y_lim[0]) * height

    return X,Y



def create_sky_grid(height,width,theta_min,theta_max,phi_min,phi_max, n):
    """
    create a rectangular grid of size (height, width) with corresponding
    projection in spherical coordinates

    The grig map to the portion of the sky (theta_min,theta_max), (phi_min_phi_max)

    theta is the angle from the zenith

    :param n: number of point horizontally and vertically
    :param height:
    :param width:
    :param theta_min:
    :param theta_max:
    :param phi_min:
    :param phi_max:
    :param n:
    :return:
    """
    # screen height and width
    # http: // mathworld.wolfram.com / GnomonicProjection.html
    # x,y = np.meshgrid(np.linspace(0, , n),
    #                          np.linspace(0, screen.get_width(), n))

    # height = screen.get_height()#q*0.8
    # width = screen.get_width()
    #
    # vertical_fov = np.array([0, 50]) / 180 * np.pi
    # horizontal_fov = np.array([-45, 45]) / 180 * np.pi
    vertical_fov = np.array([theta_min, theta_max])
    horizontal_fov = np.array([phi_min, phi_max])

    theta_0 = vertical_fov.mean()*0
    phi_0 = horizontal_fov.mean()*0

    y_lim = np.tan(vertical_fov-theta_0)
    x_lim = np.tan(horizontal_fov-phi_0)

    x, y = np.meshgrid(np.linspace(x_lim[0], x_lim[1], n),
                       np.linspace(y_lim[0], y_lim[1], n))

    X = (x.ravel() - x_lim[0])/(x_lim[1]-x_lim[0])  * width
    Y = (y.ravel() - y_lim[0])/ (y_lim[1]-y_lim[0]) * height


    theta, phi = inv_gnomonic_projection(x, y, theta_0=theta_0, phi_0=phi_0)
    theta += theta_0
    phi += phi_0

    vertices_ = np.vstack([X,Y])
    theta = theta.ravel()
    phi = phi.ravel()

    indices = []

    for i in range(n - 1):
        row_i = range((i) * (n), (i + 1) * n)
        row_ip1 = range((i + 1) * (n), (i + 2) * n)

        indices.extend(zip(row_i, row_ip1))
        indices.extend([(row_ip1[-1], row_ip1[0])])

    vertices = vertices_.T[list(itertools.chain(*indices))]

    return vertices, indices, theta,phi,

import ephem
from ephem import stars
from collections import defaultdict

def celestial_bodies_position(date,height,width,xlim,ylim,theta_0,phi_0,lon='5.747832',lat='45.202847'):
    obs = ephem.Observer()
    #obs.lon, obs.lat = '5.747832',
    obs.lon,obs.lat = lon,lat

    # height = screen.get_height()
    # # width = screen.get_width()
    # vertical_fov = np.array([theta_min, theta_max])
    # horizontal_fov = np.array([phi_min, phi_max])
    # #
    # # theta_0 = vertical_fov.mean()*0
    # # phi_0 = horizontal_fov.mean()*0
    # # # theta_0 = 0
    # # phi_0 = 0
    #
    # y_lim = np.tan(vertical_fov - theta_0)
    # x_lim = np.tan(horizontal_fov - phi_0)


    sun = ephem.Sun()
    moon = ephem.Moon()

    res = defaultdict(list)

    obs.date = date
    for star_name, star in stars.stars.items():
        star.compute(obs)

        res[star_name].append((star.alt, (star.az+np.pi)%(2*np.pi)))
    sun.compute(obs)
    moon.compute(obs)

    res['moon'].append((moon.alt, moon.az+np.pi))
    res['sun'].append((sun.alt, sun.az+np.pi))


    theta, phi = zip(*[pos[0] for star_name, pos in res.items()])
    theta = np.array(theta)
    phi = np.array(phi)
    x, y = gnomonic_projection(theta=np.array(theta), phi=np.array(phi), theta_0=theta_0, phi_0=phi_0)
    theta #+= theta_0
    phi #+= phi_0
    X = (x.ravel() - xlim)/(-xlim -xlim)  * width
    Y = (y.ravel() - ylim)/(-ylim -ylim) * height

    sun_x = X[-1]
    sun_y = Y[-1]
    moon_x = X[-2]
    moon_y = Y[-2]
    star_x = X[:-2]
    star_y = Y[:-2]
    return theta,phi, X, Y


if __name__ == "__main__":
    import matplotlib.pylab as plt
    # #import colorpy
    # from colorpy import colormodels
    # theta,gamma = np.meshgrid(np.linspace(0,2*np.pi,20),np.linspace(0,2*np.pi,20))
    #
    # colors_xyY = sky_color(theta.ravel(), gamma.ravel(), turbidity=1)
    # colors = colormodels.rgb_from_xyz(colormodels.xyz_color_from_xyY(colors_xyY[0], colors_xyY[1], colors_xyY[2]))
    # colors = np.clip(colors,0,1)
    #
    #
    #
    # nx = 1000
    # ny = nx
    # theta, phi = np.meshgrid(np.linspace(0.1, np.pi / 2, nx), np.linspace(0.01, 2 * np.pi, ny))
    # sun_theta_phi = np.array([np.pi / 4 , np.pi])  # sun south at 45° above the horizon
    #
    # rgb = sun_color(sun_theta_phi[0],turbidity=5)
    #
    #
    #
    #
    # x, y, z = sph2cart(phi.ravel(), theta.ravel(), 1)
    # sun_x, sun_y, sun_z = sph2cart(sun_theta_phi[1], sun_theta_phi[0], 1)
    #
    # gamma_ = np.arccos(np.vstack([x, y, z]).T.dot(np.array([[sun_x, sun_y, sun_z]]).T)).reshape([nx, ny])
    # theta_ = np.pi / 2 - theta
    #
    # colors_xyY  = sky_color(theta_.ravel(), gamma_.ravel(), np.pi / 2 - sun_theta_phi[0], turbidity=5)
    # #print(np.mean(colors_xyY[2]))
    # print(lum)
    # lum = max(1,np.mean(colors_xyY[2]))
    # #lum = np.median(colors_xyY[2])
    # colors_xyz  = colormodels.xyz_color_from_xyY(colors_xyY[0], colors_xyY[1], colors_xyY[2]*0.8/lum)
    # #colors_xyz = colormodels.xyz_normalize(colors_xyz)
    # colors = colormodels.rgb_from_xyz(colors_xyz)  # /255.
    # colors = np.clip(colors, 0, 1)
    #
    # #plt.scatter(x=colors_xyz[0,:],y=colors_xyz[1,:])
    # #plt.imshow( colors_xyz.T.reshape([200, 200, 3])[:, :, 0].T, origin='lower')
    # #plt.colorbar()
    # f,ax = plt.subplots(1,1)
    # ax.imshow(np.stack([colors.T.reshape([nx, ny, 3])[:, :, 0],
    #                      colors.T.reshape([nx, ny, 3])[:, :, 1],
    #                      colors.T.reshape([nx, ny, 3])[:, :, 2]]).T, origin='lower',aspect=0.5)
    #
    # ax.scatter(x=[int(nx/2)],y=[1],s=100,c=rgb,edgecolor='none')
    # #colors = np.clip().T/255.,0,None)
    # #plt.scatter(theta,gamma,s=100)#,c=colors)
