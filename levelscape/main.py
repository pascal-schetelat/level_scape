
from pathlib import Path
from pkg_resources import resource_filename
import os, sys
from datetime import datetime, timedelta

import random
import numpy as np
import pandas as pd
import pygame
import pymunk
from pymunk import Vec2d
from pymunk import pygame_util  # import to_pygame, DrawOptions

from levelscape.pyWorm2D.worm2d import Engine, Viewport
from levelscape.levels.ign import load_grenoble
from levelscape.classes import CircularBackground, Sky, SkyGrid

elev = load_grenoble()

WIN_WIDTH = 1600  # 800
WIN_HEIGHT = 1080  # 640


VFOV = 60/180*np.pi
HFOV = VFOV*WIN_WIDTH/WIN_HEIGHT

HALF_WIDTH = int(WIN_WIDTH / 2)
HALF_HEIGHT = int(WIN_HEIGHT / 2)

DISPLAY = (WIN_WIDTH, WIN_HEIGHT)

SKY_BLUE = [0. / 255, 204. / 255, 142. / 255]
SKY_YELLOW = [213. / 255, 111. / 255, 171. / 255, ]

def main():

    shift = [0, 0]
    theta_phi = [0,np.pi/10]
    zoom_factor = 1

    POV_z = 500
    POV_phi = 0
    POV_r = 1

    def to_pygame_shifted(p, surface):
        """Convenience method to convert pymunk coordinates to pygame surface
        local coordinates.

        Note that in case positive_y_is_up is False, this function wont actually do
        anything except converting the point to integers.
        """
        # print('SHIFTED!')
        if pygame_util.positive_y_is_up:
            return int(p[0]) + shift[0], surface.get_height() - int(p[1]) + shift[1]
        else:
            return int(p[0] + shift[0]), int(p[1] + shift[1])



    #====================================================================
    #                INITIALIZATIONS
    #====================================================================
    #w, h, depth, fullscreen, caption, font = None
    engine = Engine(DISPLAY[0], DISPLAY[1], depth=32, fullscreen=True, caption="levelscape")


    view = Viewport(0., 0., 1, 1, DISPLAY[0], DISPLAY[1])
    screen = engine.screen

    # Initialize the joysticks
    #pygame.joystick.init()
    #joysticks = [pygame.joystick.Joystick(i) for i in range(pygame.joystick.get_count())]

    # todo wait for joystick to be plugged
    #joystick = joysticks[0]
    #joystick.init()

    clock = pygame.time.Clock()

    #space = pymunk.Space()
    #space.gravity = (0.0, -800.0)

    #sky = Sky(date=datetime(2018,5,6,6,40),turbidity=2)#,theta_min=0.05,theta_max=np.pi/2-0.3,phi_min=0.1,phi_max=np.pi-0.1)
    sky = SkyGrid(n=100,vfov=VFOV,hfov=HFOV,height=WIN_HEIGHT,width=WIN_WIDTH,lon='5.747832',lat= '45.202847',date=datetime(2018,4,10,6,40))
    sky.set_engine(engine=engine)
    #====================================================================
    #                create levels from IGN
    #====================================================================

    backgrounds = []

    for r in elev.index.levels[0][-20::-1]:

        d = r * elev.loc[r].index / 5000 * 2 * np.pi
        z = elev.loc[r].z

        # level_length_in_meter = 14050#d.max() / 10
        # level_scale = 1 / 1600.


        #altitudes,radius,color=np.array([0,0,0]),z_order=0,
        back = CircularBackground(altitudes=z.values, radius= r,
                            color=np.array([0.10, 0.10, 0.10]),
                            z_order=-1)

        back.set_engine(engine)
        vertices = back.get_vertices().reshape((-1, 2))[1::2, :]
        backgrounds.append(back)

    #skyline = CircularSkyline(r,d,z,intrinsic_color=[1,1,1])
    #pygame.key.set_repeat(0, 1)

    pygame.mouse.get_rel()

    while True:

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    pygame.quit()



        pressed = pygame.key.get_pressed()

        delta = 1000
        if pressed[pygame.K_RIGHT]:
            #shift[0] -= delta
            theta_phi[1] -= 0.01

        if pressed[pygame.K_LEFT]:
            #shift[0] += delta
            theta_phi[1] += 0.01

        if pressed[pygame.K_DOWN] :
            #shift[1] -= delta
            if pressed[pygame.K_LCTRL ]:
                POV_r = max(POV_r-20,1)
            else :
                POV_z -= 10


        if pressed[pygame.K_UP]:
            #shift[1] += delta
            if pressed[pygame.K_LCTRL ]:
                POV_r += 20
            else :
                POV_z += 10


        if pressed[pygame.K_UP]:
            shift[1] += delta

        x,y = pygame.mouse.get_rel()

        if pygame.mouse.get_pressed()[0]:
            #print(x)
            #shift[0] = shift[0] + x*100
            #shift[1] = shift[1] + y*100

            theta_phi[0] = theta_phi[0] + y*0.0005
            theta_phi[1] = theta_phi[1] + x*0.0005
        sky.project_raw_grid(theta_0=theta_phi[0], phi_0=theta_phi[1])

        engine.PreRender(SKY_YELLOW[0], SKY_YELLOW[1], SKY_YELLOW[2])
        engine.SetView(view, DISPLAY[0] * 0.5, DISPLAY[1] * 0.5, zoom_factor, NoDepth=False)
        #t = sky.date + timedelta(minutes=0.5)
        t = sky.date + timedelta(minutes=0.5)

        sky.draw(date=t, turbidity=1.9, z_order=-10, zoom=zoom_factor)
        # sky.draw(turbidity=10, z_order=-10, zoom=zoom_factor)
        median_color = sky.median_color
        #median_color = np.array([0.5,0.5,0.8])

        for back in backgrounds:
            #back.isSelected = True
            #back.draw(shift=shift,ambiant_color=median_color)
            back.draw(theta_phi= theta_phi, r=POV_r, z=POV_z, vfov=VFOV, hfov=HFOV,
                        ambiant_color = median_color)

        bb_r_h = 0.40
        bb_r_v = 0.2
        BOUNDING_BOX = (
        (DISPLAY[0] * (bb_r_h), DISPLAY[1] * (1 - 0.4)), (DISPLAY[0] * (1 - bb_r_h), DISPLAY[1] * (1 - 0.1)))
        # print(BOUNDING_BOX)

        step = 1.0 / 30
        substep = 2  # each frame is 5 steps
        #for x in range(substep):  # move simulation forward 0.1 seconds:
        #    space.step(step / substep)

        pygame.display.flip()
        # pygame.display.update()

        clock.tick(60)
        # theta,phi = sky.sun.get_direction()
        # date = sky.date.ctime()
        pygame.display.set_caption(f"time : {t.strftime('%-d %B %Y %Hh%M')} |fps: {int(clock.get_fps()):d} | direction : {int(theta_phi[1]*180/np.pi):d}")


if __name__ == '__main__':
    main()
