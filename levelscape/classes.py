import numpy as np
from levelscape.levels.landscape import create_circular_skyline
from levelscape.sky.sky_models import create_sky_grid, simulate_color, haversine_np, inv_gnomonic_projection
from datetime import datetime
from levelscape.object_builder import *
from colorpy import colormodels,  ciexyz
import itertools
try :
    import matplotlib.pyplot as plt
except ImportError :
    pass


def solve_gamma(r, R, hfov):
    """
    find gamma, the apparent angle from the center of the field of view as seen from distance r to the center of
    the circle of radius R.
    :param r: point of view distance from the center
    :param R: great circles  radius
    :param hfov: horizontal field of view
    :return:
    """
    return 2 * np.arccos(np.roots([1, -2 * r / R * (np.sin(hfov / 2)) ** 2,
                                   -1 + np.sin(hfov / 2) ** 2 * (1 + (r / R) ** 2)])) #/ np.pi * 180



class PolygonObject(object):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        pass

    def set_engine(self, engine):
        self.engine = engine
        self.screen = self.engine.screen
        self.DISPLAY = (self.screen.get_width(), self.screen.get_height())

        self.register_vertices()

    def get_vertices(self):
        return self.vertices  # not Implemented Error
        # pass

    def register_vertices(self):
        self.get_vertices()
        self.poly_v_gl = self.engine.LoadPolygon(self.vertices)

    def register_colors(self):
        self.color_v_gl = self.engine.LoadColor(self.get_colors())






class TextureObject(object):
    def __init__(self, image_path, image_height, z_order=0, y_offset=True, x_offset=False, **kwargs):
        """
        Images must we white with an alpha chanel for transparency
        :param image_path:
        :param image_height: in meters

        """
        super().__init__(**kwargs)
        self.image_path = image_path
        self.image_height = image_height
        self.z_order = z_order
        self.x_offset = x_offset
        self.y_offset = y_offset

    def set_engine(self, engine):
        self.engine = engine
        self.screen = self.engine.screen
        self.DISPLAY = (self.screen.get_width(), self.screen.get_height())
        self.register_image()

    def register_image(self):
        self.image = self.engine.LoadImage(self.image_path)
        self.image_size = self.image.rx, self.image.ry

        self.foreground_scaling_factor = self.image_height / level_scale / self.image_size[1]

        self.y_offset = self.image_size[1] / 2 * self.foreground_scaling_factor
        self.x_offset = self.image_size[0] / 2 * self.foreground_scaling_factor

        # level_scale -> 1/160. # in meters by pixels -> here 160px by meters

    def draw(self, x, y, scale, color=np.array([0, 0, 0]), z_order=None):
        """

        :param x: screen position
        :param y:
        :param color: rgb array
        :param z_order:
        :return:
        """

        if not hasattr(self, 'image'):
            self.register_image()

        if z_order is not None:
            self.z_order = z_order

        self.engine.DrawImage(image=self.image,
                              x=x + self.x_offset * scale, y=y + 0 * self.y_offset * scale,
                              w=self.foreground_scaling_factor * scale,
                              h=self.foreground_scaling_factor * scale,
                              a=1, rot=0,
                              r=color[0],
                              g=color[1],
                              b=color[2],
                              UseRatio=True, Cull=False, z=self.z_order)


level_length_in_meter = 450
level_scale = 1 / 160.


class Background(PolygonObject):
    """
    Plane is purely a vertex, it is never simulated
    """
    childClasses = [TextureObject]

    def __init__(self,distance,length,altitudes,level_scale,color=np.array([0,0,0]),z_order=0,**kwargs):
        """

        :param distance: distance fro mthe foreground in m
        :param altitudes: in m
        :param color: intrinsic color of the background
        """
        #super().__init__(distance, z_order,**kwargs)
        self.vertices = None
        self.length_in_meter = length
        self.level_scale = level_scale
        self.altitudes = altitudes
        self.distance = distance
        self.intrinsic_color = color
        self.z_order = z_order
        self.images = {}

    #def set_projection
        # decoupling from screen

    def set_engine(self,engine):
        super().set_engine(engine)

        for k,v in self.images.items() :
            if not hasattr(v['image'],'engine'):
                v['image'].set_engine(engine)


    def add_image(self,image_path,image_height,x,y):
        """

        :param image_path: path to an image (white with transparency )
        :param image_height: in meters
        :param x: in screen coordinates. use background vertices to fill those
        :param y:
        :return:
        """

        if image_path not in self.images :
            image  = TextureObject(image_path=image_path,image_height=image_height)
            if hasattr(self, 'engine'):
                image.set_engine(self.engine)

            self.images[image_path] = {"image" : image,
                                       "positions" : [(x,y)]}

        else :
            self.images[image_path]['positions'].append((x,y))



    def get_vertices(self):
        self.vertices, self.scale = create_skyline_vertices_from_serie_triangle_strip(self.altitudes,
                                                                                      self.distance,
                                                                                      self.length_in_meter, self.level_scale,
                                                                                      self.DISPLAY[0],self.DISPLAY[1])

        return self.vertices

    def draw(self,shift,ambiant_color=np.array([1,1,1]),turbidity=12):
        factor = 0.15# *(1+ 3*turbidity)
        proportion = np.clip(self.scale**factor,0,0.9)
        #print(ambiant_color)
        plane_color = ambiant_color * (1-proportion) + self.intrinsic_color * (proportion)

        self.engine.DrawPoly(self.poly_v_gl,    x=shift[0] * self.scale,
                                                y=shift[1] * self.scale,
                                                w=1, h=1, a=1, rot=0,
                                                r=plane_color[0],
                                                g=plane_color[1],
                                                b=plane_color[2],
                                                z=self.z_order, mode='GL_TRIANGLE_STRIP')

        for image_path,image_dict in self.images.items() :
            img  = image_dict['image']
            for pos in image_dict['positions']:
                x,y = pos
                image_dict['image'].draw(x=x+self.scale*shift[0],
                                         y=y+self.scale*shift[1],
                                         #scale=self.scale,color=plane_color,z_order=self.z_order)
                                         scale=self.scale, color=plane_color, z_order=self.z_order)


####

class  CircularBackground(Background):

    def __init__(self,altitudes,radius,color=np.array([0,0,0]),z_order=0,**kwargs):
        """

        :param radius: distance from the center
        :param altitudes: in m
        :param color: intrinsic color of the background
        """
        #super().__init__(distance, z_order,**kwargs)
        self.vertices = None
        self.radius = radius
        self.altitudes = altitudes
        self.intrinsic_color = color
        self.z_order = z_order
        self.images = {}

    def get_vertices(self):
        self.vertices = create_circular_skyline(self.altitudes)


        return self.vertices

    def draw(self,theta_phi,r,z,vfov,hfov,ambiant_color=np.array([1,1,1])):
        """

        :param theta_phi: spherical coordinates of the view direction. phi is bound to the orientation of the circle
        :param r: radius of the point of view
        :param z: altitude of the point of view
        :param vfov: vertical field of view
        :param hfov: horizontal field of view
        :return:
        """
        if self.radius < r+10:
            return

        self.isSelected = False
        if self.isSelected:
            self.mode = 'GL_LINE_STRIP'
        else :
            self.mode = 'GL_TRIANGLE_STRIP'

        # gamma is the angle of the great circle view from the point of view with field of view hfov
        gamma = solve_gamma(r, self.radius, hfov)[0]
        # v_scale : how big a meter seen from (R-r) is displayed on the screen
        v_scale = np.arctan(1/(self.radius-r))/ vfov * self.DISPLAY[1]
        h_scale =                   self.DISPLAY[0]/gamma
        v_shift = -z*v_scale
        h_shift = theta_phi[1] * h_scale
        theta_shift = theta_phi[0] / vfov * self.DISPLAY[1]

        factor = 0.8
        proportion = np.clip(v_scale ** factor, 0, 0.9)
        plane_color = ambiant_color * (1-proportion) + self.intrinsic_color * (proportion)

        self.engine.DrawPoly(self.poly_v_gl,    x=h_shift+self.DISPLAY[0]/2,
                                                y=self.DISPLAY[1]-v_shift+theta_shift-self.DISPLAY[1]/2,
                                                w=h_scale, h=-v_scale, a=1, rot=0,
                                                #w=1, h=1, a=1, rot=0,
                                                r=plane_color[0],
                                                g=plane_color[1],
                                                b=plane_color[2],
                                                z=self.z_order, mode=self.mode)




from levelscape.sky.sky_models import celestial_bodies_position

class SunDisk(PolygonObject):
    def __init__(self, date=datetime(2018,5,6,12,30),lon=5.747832,lat= 45.202847,turbidity=1,n=30):
        self.lon = lon
        self.lat = lat
        self.n = n
        self.turbidity = turbidity
        self.colors = None
        self.color=np.array([[0,0,0,]])
        self.set_date(date)
        #self.register_vertices()

    def set_date(self,date):
        self.date = date
        # self.altitude = np.pi/2-get_altitude(self.lat,self.lon, self.date)*np.pi/180
        # #self.azimuth  = np.mod(get_azimuth(self.lat,self.lon, self.date)*np.pi/180,2*np.pi)
        # self.azimuth = -get_azimuth(self.lat, self.lon, self.date) * np.pi / 180
        self.get_color()


    def get_color(self):
        #self.color = sun_color(self.altitude,self.turbidity)
        self.color = np.array([[1,1,1,]])
        return self.color

    def get_direction(self):
        if not hasattr(self,'altitude'):
            self.get_screen_position()
        return self.altitude, self.azimuth

    def get_screen_position(self):
        theta, phi, X, Y = celestial_bodies_position(self.date,height= self.parent_sky.screen.get_height(),
                                                               width = self.parent_sky.screen.get_width(),
                                                                theta_0 = self.parent_sky.theta_0, phi_0 = self.parent_sky.phi_0,
                                                                xlim = self.parent_sky.xlim,ylim = self.parent_sky.ylim,
                                                                                 lon=self.lon,lat=self.lat)
        self.altitude = theta[-1]
        self.azimuth = phi[-1]
        # self.altitude =  -theta[-1]*2+ np.pi / 2
        # self.azimuth = phi[-1]
        return X[-1],Y[-1]#self.parent_sky.screen.get_height()-Y[-1]

    def get_vertices(self):
        #x,y = self.get_screen_position()

        self.nodes_coords,_ = blob_vertices(x=0, y=0, radius=40, n=self.n)
        #self.vertices =
        self.vertices = np.array(self.nodes_coords).ravel()
        return self.vertices

    def draw(self,date=None,zoom=1,z_order=0):
        if date is not None :
            self.date = date
        x,y = self.get_screen_position()

        self.engine.DrawPoly(self.poly_v_gl,
                             x=x,#*zoom-0.5 * (zoom - 1) * self.DISPLAY[0],
                             y=y,#*zoom-(zoom - 1) * self.DISPLAY[1],
                        #x=x,#*zoom- 0.5 * (zoom - 1) * self.DISPLAY[0],
                        #y=y,#*zoom-      (zoom - 1) *self.DISPLAY[1],
                        w=1, h=1, a=1, rot=0,
                        r=1,
                        g=1,
                        b=1, z=z_order, mode='GL_TRIANGLE_FAN')


class SkyGrid(PolygonObject):
    def __init__(self, n, vfov, hfov, height, width, lon, lat,date):
        """
        n : number of point by dimension
        vfov : vertical field of view (in radian)
        hfov : horizontal field of view (in radian)
        height : screen height in pixels
        width : screen width in pixels
        lon, lat : sky position on earth

        return a list of dimensionless vertices centered around (0,0)

        beware, if the height / width ratio is different from hfov/vfov, image will be skewed
        it should be enought to call this once at initialisation, just before vertices registration
        """
        self.n = n
        self.hfov = hfov
        self.vfov = vfov
        self.create_raw_grid()
        # initialize altitude and azimuth for color calculation
        self.project_raw_grid(theta_0=0, phi_0=0)
        self.create_indices()
        self.width = width
        self.height = height
        self.lat = lat
        self.lon = lon
        self.date = date
        self.sun = SunDisk(date=self.date,lon=lon,lat=lat,turbidity=1)
        self.sun.parent_sky = self

    def set_engine(self, engine):

        super(SkyGrid,self).set_engine(engine)
        self.sun.set_engine(engine)

    def create_raw_grid(self, lower_limit=0.8):

        self.xlim = haversine_np(0, 0, 0, self.hfov )
        self.ylim = haversine_np(0, 0, self.vfov / 2, 0) # ???

        self.x, self.y = np.meshgrid(np.linspace(-self.xlim, self.xlim, self.n),
                                     np.linspace(-self.ylim, self.ylim, self.n))

        # mask to estimate horizon median color
        self.lower_pixels_mask = self.y.ravel() < -lower_limit * self.ylim

    def create_indices(self):
        indices = []

        for i in range(self.n - 1):
            row_i = range((i) * (self.n), (i + 1) * self.n)
            row_ip1 = range((i + 1) * (self.n), (i + 2) * self.n)

            indices.extend(zip(row_i, row_ip1))
            indices.extend([(row_ip1[-1], row_ip1[0])])

        self.indices = indices

    def project_raw_grid(self, theta_0, phi_0):
        """
        Project the screen raw grid on spherical coordinates centered around theta_0, phi_0
        This is used each time we change the field of view orientation
        theta and phi are like latitude and longitude on earth
        """
        self.theta_0 = theta_0
        self.phi_0 = phi_0

        self.theta, self.phi = inv_gnomonic_projection(self.x.ravel(), self.y.ravel(),
                                                       theta_0=self.theta_0, phi_0=phi_0)

        self.theta_min = self.theta.ravel().min()
        self.phi_min   = self.phi.ravel().min()
        self.theta_max = self.theta.ravel().max()
        self.phi_max   = self.phi.ravel().max()

        return self.theta, self.phi

    def set_date(self,date):
        #if date != self.date :
        self.date = date
        self.sun.set_date(date)

    def get_vertices(self):

        X = (self.x.ravel() - self.xlim) / (-self.xlim - self.xlim) * self.width
        Y = (self.y.ravel() - self.ylim) / (-self.ylim - self.ylim) * self.height

        vertices_ = np.vstack([X, Y])

        self.vertices = vertices_.T[list(itertools.chain(*self.indices))]

        return self.vertices

    def get_colors(self, date, turbidity=2, theta_0=None, phi_0=None):
        self.date = date

        if (theta_0 is not None) or (phi_0 is not None):
            self.project_raw_grid(theta_0, phi_0)

        self.sun.get_screen_position()
        sun_theta, sun_phi = self.sun.get_direction()

        return self._get_colors(sun_theta=sun_theta, sun_phi=sun_phi,
                                turbidity=turbidity)

    def _get_colors(self, sun_theta, sun_phi, turbidity=2):

        self.colors, self.colors_xyz, lum = simulate_color(np.pi / 2 - self.theta, self.phi,
                                                           (np.pi / 2 - sun_theta, sun_phi), turbidity)
        self.colors = self.colors.T
        self.median_color = colormodels.rgb_from_xyz(self.colors_xyz[self.lower_pixels_mask, :].mean(axis=0))
        # self.median_color = color_regularisaton(self.median_color)
        return self.colors[self.indices]

    def draw(self,date,turbidity=2,z_order=0,zoom=1):
        self.set_date(date)
        self.isSelected = False
        if self.isSelected:
            self.mode = 'GL_LINE_STRIP'
        else :
            self.mode = 'GL_TRIANGLE_STRIP'

        self.engine.DrawPoly(self.poly_v_gl, x=0,#0.5*(zoom-1)*self.DISPLAY[0],
                                             y=0, #-(zoom-1)*self.DISPLAY[1],
                             w=zoom, h=zoom, a=1, rot=0,
                             r=1,
                             g=0,
                             b=0,
                             colors = self.get_colors(date,turbidity=turbidity),
                             #z=z_order, mode='GL_TRIANGLE_STRIP')
                             z=z_order, mode=self.mode)

        if date is not None:
            self.set_date(date)
        self.sun.draw(zoom=zoom,z_order=z_order)

    def plot_colors(self, sun_theta, sun_phi, turbidity=2, coordinates='screen', **kwargs):
        options = {'edgecolor': 'none', 's': 20}
        options.update(kwargs)
        kwargs = options
        self._get_colors(sun_theta=sun_theta, sun_phi=sun_phi, turbidity=turbidity)
        f = plt.figure()  # figsize=[10,10])

        if coordinates == 'screen':
            ax = f.add_subplot(111)
            ax.scatter(x=self.x, y=self.y, c=self.colors, **kwargs)
        if coordinates == 'spherical':
            ax = f.add_subplot(111)
            ax.scatter(x=self.phi, y=self.theta, c=self.colors, **kwargs)
            ax.set_xlim([-np.pi, np.pi])
            ax.set_ylim([-np.pi / 2, np.pi / 2])

        if coordinates == '3D':
            r = 0.9
            pi = np.pi
            cos = np.cos
            sin = np.sin
            theta, phi = np.meshgrid(np.linspace(-np.pi / 2, np.pi / 2, 30),
                                     np.linspace(-np.pi, np.pi, 30))
            x = r * cos(theta) * cos(phi)
            y = r * cos(theta) * sin(phi)
            z = r * sin(theta)

            xx = np.cos(self.theta) * np.cos(self.phi)
            yy = np.cos(self.theta) * np.sin(self.phi)
            zz = np.sin(self.theta)

            # Set colours and render
            # fig = plt.figure()
            ax = f.add_subplot(111, projection='3d')

            ax.plot_surface(
                x, y, z, rstride=1, cstride=1, color='k', alpha=0.3, linewidth=0)

            #gamma = haversine_np_grid(self.phi, self.theta, sun_phi, sun_theta)

            ax.scatter(xx, yy, zz, c=self.colors, s=50, alpha=1)
            # ax.scatter(xx,yy,zz,c=gamma,s=50,alpha=1,cmap='Blues')

            ax.set_xlim([-1, 1])
            ax.set_ylim([-1, 1])
            ax.set_xlabel("x")
            ax.set_ylabel("y")
            ax.set_zlabel("z")
            ax.set_aspect("equal")
            return ax

    #             ax.scatter(x=self.phi,y=self.theta,c=self.colors,**kwargs)
    #             ax.set_xlim([-np.pi,np.pi])
    #             ax.set_ylim([-np.pi/2,np.pi/2])

    #         ax.invert_yaxis()
    #         return ax

    def plot_spherical_grid(self):
        f = plt.figure()
        ax = f.add_subplot(111)
        ax.scatter(x=self.phi, y=self.theta, s=1)
        ax.set_xlabel('phi')
        ax.set_ylabel('theta')
        # ax.set_xlim([-np.pi,np.pi])
        # ax.set_ylim([0,np.pi])
        # ax.invert_yaxis()
        return ax


class Sky(PolygonObject):
    """
    Sky object to draw as a background

    """
    def __init__(self,date=datetime(2018,5,6,12,30),lon=5.747832,lat= 45.202847,
                 turbidity=6,n=100,
                 theta_min=0.1,theta_max=60/180*np.pi,
                 phi_min=30/180*np.pi, phi_max=150 / 180 * np.pi):
        """

        The sky is a grid of triangles that cover a rectangle in pixel coordinates
        Each node of the grid as a corresponding coordinate in spherical coordinate system

        Each node angles (theta, phi) are used with the sun position to calculate the sky color
        It is then interpolated inside each triangle based on each vertex color value.

        Conversion between screen coordinates and spherical coordinates is done via gnomomic projection

        the sun position is estimated PyEphem

        :param date:  date for sun position
        :param lon: position for sun position
        :param lat: position for sun position
        :param turbidity: relate to how much particles are in the atmosphere. 1 is clear sky, 12 is very hazy.
        :param n: number of triangles
        :param theta_min: bounds of the spherical window to project on the screen
        :param theta_max: bounds of the spherical window to project on the screen
        :param phi_min: bounds of the spherical window to project on the screen
        :param phi_max: bounds of the spherical window to project on the screen
        """
        # self.lon = lon
        # self.lat = lat
        self.date = date
        self.colors = None
        self.turbidity = turbidity
        self.theta_min = theta_min
        self.theta_max = theta_max
        self.phi_min = phi_min
        self.phi_max = phi_max

        self.sun = SunDisk(date=self.date,lon=5.747832,lat= 45.202847,turbidity=1)
        self.sun.parent_sky = self


    def set_engine(self,engine):
        super(Sky,self).set_engine(engine)
        self.sun.set_engine(engine)


    def get_sun_position(self,date=None):
        return self.sun.get_screen_position(date=date)
        # if date is not None:
        #     self.date = date
        # self.altitude = np.pi/2-get_altitude(self.lat,self.lon, self.date)*np.pi/180
        # self.azimuth  = np.mod(get_azimuth(self.lat,self.lon, self.date,elevation=0)*np.pi/180,2*np.pi)
        # return self.altitude, self.azimuth

    def set_turbidity(self,turbidity):
        self.turbidity= turbidity

    def get_vertices(self):
        self.vertices,self.indices, self.theta, self.phi = create_sky_grid(height=self.screen.get_height(),
                                                                           width=self.screen.get_width(),
                                                                            theta_min  = self.theta_min,
                                                                            theta_max  = self.theta_max,
                                                                            phi_min    = self.phi_min,
                                                                            phi_max    = self.phi_max,
                                                                           n=20)
        self.lower_pixels_mask = (self.theta>(np.pi/2*0.65))&(self.theta<(np.pi/2*0.9))
        return self.vertices

    def set_date(self,date):
        if date != self.date :
            self.date = date
            self.sun.set_date(date)

    # def get_rayleigh_color(self):
    #     illuminant = illuminants.get_blackbody_illuminant(blackbody.SUN_TEMPERATURE)
    #     color_xyz = rayleigh_illuminated_color(illuminant)
    #     self.rayleigh_color = rgb_from_xyz(color_xyz)
    #     return self.rayleigh_color

    def get_colors(self,date=None):
        colors = self._get_colors(date=date, turbidity=self.turbidity, theta=self.theta, phi=self.phi)
        self.median_color = colormodels.rgb_from_xyz(self.colors_xyz[self.lower_pixels_mask,:].mean(axis=0))
        #self.median_color = color_regularisaton(self.median_color)
        return colors[self.indices]

    def _get_colors(self,theta,phi,date=None,turbidity=2):

        if self.colors is not None:
            if (date == self.date) and (turbidity== self.turbidity):
                return self.colors

        if date is not None :
            self.set_date(date)

        self.colors,self.colors_xyz,lum = simulate_color(theta, phi, self.sun.get_direction(), turbidity)
        self.colors = self.colors.T
        #n = len(self.theta)


        return self.colors


    def draw(self,date=None,turbidity=2,z_order=0,zoom=1):
        self.isSelected = False
        if self.isSelected:
            self.mode = 'GL_LINE_STRIP'
        else :
            self.mode = 'GL_TRIANGLE_STRIP'

        self.engine.DrawPoly(self.poly_v_gl, x=0,#0.5*(zoom-1)*self.DISPLAY[0],
                                             y=0, #-(zoom-1)*self.DISPLAY[1],
                             w=zoom, h=zoom, a=1, rot=0,
                             r=1,
                             g=0,
                             b=0,
                             colors = self.get_colors(date),
                             #z=z_order, mode='GL_TRIANGLE_STRIP')
                             z=z_order, mode=self.mode)

        if date is not None:
            self.set_date(date)
        self.sun.draw(zoom=zoom,z_order=z_order)