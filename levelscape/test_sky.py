
from pathlib import Path
from pkg_resources import resource_filename
import os, sys
from datetime import datetime, timedelta

import random
import numpy as np
import pandas as pd
import pygame
import pymunk
from pymunk import Vec2d
from pymunk import pygame_util  # import to_pygame, DrawOptions

from levelscape.pyWorm2D.worm2d import Engine, Viewport
from levelscape.levels.ign import load_grenoble
from levelscape.classes import Background, Sky

elev = load_grenoble()

WIN_WIDTH = 1600  # 800
WIN_HEIGHT = 1080  # 640
HALF_WIDTH = int(WIN_WIDTH / 2)
HALF_HEIGHT = int(WIN_HEIGHT / 2)

DISPLAY = (WIN_WIDTH, WIN_HEIGHT)

SKY_BLUE = [0. / 255, 204. / 255, 142. / 255]
SKY_YELLOW = [213. / 255, 111. / 255, 171. / 255, ]

def main():

    shift = [0, 0]
    zoom_factor = 1

    def to_pygame_shifted(p, surface):
        """Convenience method to convert pymunk coordinates to pygame surface
        local coordinates.

        Note that in case positive_y_is_up is False, this function wont actually do
        anything except converting the point to integers.
        """
        # print('SHIFTED!')
        if pygame_util.positive_y_is_up:
            return int(p[0]) + shift[0], surface.get_height() - int(p[1]) + shift[1]
        else:
            return int(p[0] + shift[0]), int(p[1] + shift[1])



    #====================================================================
    #                INITIALIZATIONS
    #====================================================================

    engine = Engine(DISPLAY[0], DISPLAY[1], 32, False, "levelscape")

    view = Viewport(0., 0., 1, 1, DISPLAY[0], DISPLAY[1])
    screen = engine.screen

    clock = pygame.time.Clock()

    #space = pymunk.Space()
    #space.gravity = (0.0, -800.0)

    sky = Sky(date=datetime(2018,5,6,6,40),turbidity=2,theta_min=0.1,theta_max=(np.pi/2-0.1),phi_min=-np.pi,phi_max=np.pi-0.1)
    sky.set_engine(engine=engine)

    while True:

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    pygame.quit()



        engine.PreRender(SKY_YELLOW[0], SKY_YELLOW[1], SKY_YELLOW[2])
        engine.SetView(view, DISPLAY[0] * 0.5, DISPLAY[1] * 0.5, zoom_factor, NoDepth=False)

        t = sky.date + timedelta(minutes=0.5)
        sky.draw(date=t,z_order=-10, zoom=zoom_factor)
        # sky.draw(turbidity=10, z_order=-10, zoom=zoom_factor)
        median_color = sky.median_color


        step = 1.0 / 30
        substep = 2  # each frame is 5 steps
        #for x in range(substep):  # move simulation forward 0.1 seconds:
        #    space.step(step / substep)

        pygame.display.flip()
        # pygame.display.update()

        clock.tick(60)
        # theta,phi = sky.sun.get_direction()
        # date = sky.date.ctime()
        pygame.display.set_caption(f"time : {t.strftime('%-d %B %Y %Hh%M')} |fps: {int(clock.get_fps()):d} ")

if __name__ == '__main__':
    main()
