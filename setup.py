from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
import sys
import os

from setuptools import Extension
#import numpy as np


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest

        errcode = pytest.main(self.test_args)
        sys.exit(errcode)


__version__ = '0.2'
with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='levelscape',
    version=__version__,
    author='Pascal Schetelat',
    author_email='',
    packages=find_packages(),
    url='',
    license='See LICENSE.txt',
    description=u"simple platformer in opengl using pygame and pymunk",
    package_data={'blobformer': ['.resources/*.png','sound/*.wav' ], },
    long_description=open('README.md').read(),
   # install_requires=requirements

)
